import time
import urllib.request
import asyncio
import aiohttp # async python http client lib.
import os

from urls import URLS as urls

cwd = os.getcwd()
aysnc_files_dir = os.path.join(cwd, "async_files")
sync_files_dir = os.path.join(cwd, "sync_files")


if not os.path.exists(aysnc_files_dir):
    os.makedirs(aysnc_files_dir)

if not os.path.exists(sync_files_dir):
    os.makedirs(sync_files_dir)

def fetch_sync(url):
    print('Fetch sync process {} started'.format(url))
    start = time.time()
    response = urllib.request.urlopen(url)
    filename = os.path.basename(url)
    with open(os.path.join(sync_files_dir, filename), 'wb') as f_handle:
            f_handle.write(response.read())
    print('Process {}:, took: {:.2f} seconds'.format(
        url, time.time() - start))

async def aiohttp_get(url):
    async with aiohttp.ClientSession() as session:
        async with session.get(url) as response:
            return await response.read()

async def fetch_async(url):
    print('Fetch async process {} started'.format(url))
    start = time.time()
    response = await aiohttp_get(url)
    filename = os.path.basename(url)
    with open(os.path.join(aysnc_files_dir, filename), 'wb') as f_handle:
        f_handle.write(response)
    print('Process {}:, took: {:.2f} seconds'.format(
        url, time.time() - start))
    
def synchronous():
    start = time.time()
    for i in urls:
        fetch_sync(i)
    print("Process took: {:.2f} seconds".format(time.time() - start))


async def asynchronous():
    start = time.time()
    tasks = [fetch_async(i) for i in urls]
    await asyncio.gather(*tasks)
    print("Process took: {:.2f} seconds".format(time.time() - start))


print('Synchronous:')
synchronous()

print('Asynchronous:')
asyncio.run(asynchronous())
