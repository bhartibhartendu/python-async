# python-async

This is the sample project for async programing in python.

# Project setup
- Requires Python >=3.7
- pip install -r requirements.txt
- There is a sample program which compare large number of images download using sync and async func.

# Python asynchronous programing refrence links
- https://hackernoon.com/asynchronous-python-45df84b82434
- https://dev.to/welldone2094/async-programming-in-python-with-asyncio-12dl
- https://docs.python.org/dev/library/asyncio.html